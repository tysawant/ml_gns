import os
import numpy as np
import cv2
import json
from tqdm import tqdm


def undistort_images(root_path:str, dst_path:str, save_masks=True, draw_ellipses=False):
    '''

    Parameters
    ----------
    root_path : str
        Path to the recorded images along with the calibration files.
    dst_path : str
        Path where the undistorted images and updated blobs file will be stored.
    save_masks : bool, optional
        Flag for saving/discarding binary masks. 
        The default is True.
    draw_ellipses : bool, optional
        Flag for drawing ellipses on the corrected images. 
        The default is False.

    Returns
    -------
    None.

    '''

    ####################################################################################
    # Read the blobs file to get list of ellipses in each recorded frame
    ####################################################################################
    blobs = os.path.join(root_path,"HS-P8_blobs.json")
    with open(blobs, "r") as blobs_data:
        data = json.load(blobs_data)
    
    ####################################################################################
    # Separate the left and right camera frames
    ####################################################################################
    left_camera = data["cameras"][0]["frames"]
    right_camera = data["cameras"][1]["frames"]
    
    
    ####################################################################################
    # Create destination directories for storing undistorted images
    ####################################################################################
    prefixed = os.listdir(os.path.join(root_path,"CameraFrameSaver","HS-P8"))
    if not os.path.exists("{}/images/left".format(dst_path)):
        os.makedirs("{}/images/left".format(dst_path))
    if not os.path.exists("{}/images/right".format(dst_path)):
        os.makedirs("{}/images/right".format(dst_path))
        
    ####################################################################################
    # Read the calibration file for intrinsic camera parameters
    ####################################################################################
    calib = os.path.join(root_path,"HS-P8_optical_calibration.json") 
    with open(calib, "r") as calib_data:
        data = json.load(calib_data)
        
    ####################################################################################
    # Store the left and right intrinsics and get the distortion matrices
    ####################################################################################
    left_cam_in = data["cameras"][0]["intrinsics"]
    right_cam_in = data["cameras"][1]["intrinsics"]
    
    (h, w) = (data["cameras"][0]["resolution_wh"][0], 
              data["cameras"][0]["resolution_wh"][1])
    
        
    mtx_l = cv2.UMat(np.array([[left_cam_in["f_xy"][0],0,left_cam_in["center_uv"][0]],
                               [0,left_cam_in["f_xy"][1],left_cam_in["center_uv"][1]],
                               [0,0,1]]))
    mtx_r = cv2.UMat(np.array([[right_cam_in["f_xy"][0],0,right_cam_in["center_uv"][0]],
                               [0,right_cam_in["f_xy"][1],right_cam_in["center_uv"][1]],
                               [0,0,1]]))
    
    dist_l = cv2.UMat(np.array([left_cam_in["radial"][0],
                             left_cam_in["radial"][1],
                             left_cam_in["tangential"][0],
                             left_cam_in["tangential"][1],
                             left_cam_in["radial"][2]]))
    dist_r = cv2.UMat(np.array([right_cam_in["radial"][0],
                             right_cam_in["radial"][1],
                             right_cam_in["tangential"][0],
                             right_cam_in["tangential"][1],
                             right_cam_in["radial"][2]]))
    
    ####################################################################################
    # Initialize the maps for undistoring the images
    ####################################################################################
    mapx_l, mapy_l = cv2.initUndistortRectifyMap(mtx_l, dist_l, None, mtx_l, (w,h), 5)
    mapx_r, mapy_r = cv2.initUndistortRectifyMap(mtx_r, dist_r, None, mtx_r, (w,h), 5)
        
    ####################################################################################
    # Create a new dictionary for storing the new ellipse coordinates
    ####################################################################################
    updated_blobs = {"cameras": {"left camera": [], "right camera": []}}
     
    ####################################################################################
    # Iterate through each frame and undistort the images 
    ####################################################################################      
    for frame in tqdm(list(zip(left_camera,right_camera))):
        img_name = [filename for filename in prefixed if filename.startswith(str(frame[0]["frame_id"]))]
        im = cv2.imread(os.path.join(root_path,"CameraFrameSaver","HS-P8",img_name[0]))
        
        left_img = im[0: 1080, 0:1440]
        left_el = left_img.copy()
        h,  w = left_img.shape[:2]
        right_img = im[0: 1080, 1440:2880]
        right_el = right_img.copy()
        
        ####################################################################################
        # Filter out stray ellipses by selecting ellipses with an approximate area term 
        # around the median value and draw ellipses
        ####################################################################################
        maj_min = []
        for roi in frame[0]["rois"]:
            maj_min.append((int(roi['ellipse_minor']*roi['ellipse_major'])))
        med = np.median(maj_min)
        
        for roi in frame[0]["rois"]:
            ar = int(roi['ellipse_minor']*roi['ellipse_major'])
            if abs(ar-med) < 0.5*med:
                left_el = cv2.ellipse(left_el, (int(roi['centroid_x']),int(roi['centroid_y'])), 
                                      (int(roi['ellipse_minor']),int(roi['ellipse_major'])), 
                                      int(roi['ellipse_angle']), 0, 360, (0,255,0), -1)
          
        maj_min = []
        for roi in frame[1]["rois"]:
            maj_min.append((int(roi['ellipse_minor']*roi['ellipse_major'])))
        med = np.median(maj_min)
    
        for roi in frame[1]["rois"]:
            ar = int(roi['ellipse_minor']*roi['ellipse_major'])
            if abs(ar-med) < 0.5*med:
                right_el = cv2.ellipse(right_el, (int(roi['centroid_x']),int(roi['centroid_y'])), 
                                       (int(roi['ellipse_minor']),int(roi['ellipse_major'])), 
                                       int(roi['ellipse_angle']), 0, 360, (0,255,0), -1)
    
            
        ####################################################################################
        # Rotate the images counterclockwise to display them in the proper orientation
        ####################################################################################
        left_el = cv2.rotate(left_el, cv2.ROTATE_90_COUNTERCLOCKWISE)
        right_el = cv2.rotate(right_el, cv2.ROTATE_90_COUNTERCLOCKWISE)
        left_img = cv2.rotate(left_img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        right_img = cv2.rotate(right_img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        
        ####################################################################################
        # Undistort the images with and without ellipses
        ####################################################################################
        dst_l = cv2.remap(left_img, mapx_l, mapy_l, cv2.INTER_LINEAR)
        dst_r = cv2.remap(right_img, mapx_r, mapy_r, cv2.INTER_LINEAR)
        undistorted_left_el = cv2.remap(left_el, mapx_l, mapy_l, cv2.INTER_LINEAR)    
        undistorted_right_el = cv2.remap(right_el, mapx_l, mapy_l, cv2.INTER_LINEAR)
        
        ####################################################################################
        # Convert the undistorted images with drawn ellipses into binary masks 
        # and get the contours from the undistorted masks
        ####################################################################################
        undistorted_left_el = undistorted_left_el.get()
        undistorted_right_el = undistorted_right_el.get()
        mask_l = np.zeros((w,h), dtype=bool)
        mask_r = np.zeros((w,h), dtype=bool)
        mask_l |= (undistorted_left_el == (0,255,0)).all(-1)
        mask_r |= (undistorted_right_el == (0,255,0)).all(-1)
        undistorted_left_el[~mask_l] = (0,0,0)
        undistorted_right_el[~mask_r] = (0,0,0)
        
        
        undistorted_left_el = cv2.cvtColor(undistorted_left_el, cv2.COLOR_BGR2GRAY)
        thresh, undistorted_left_el = cv2.threshold(undistorted_left_el, 127, 255, cv2.THRESH_BINARY)
        undistorted_right_el = cv2.cvtColor(undistorted_right_el, cv2.COLOR_BGR2GRAY)
        thresh, undistorted_right_el = cv2.threshold(undistorted_right_el, 127, 255, cv2.THRESH_BINARY)
        if save_masks:
            if not os.path.exists("{}/masks/left".format(dst_path)):
                os.makedirs("{}/masks/left".format(dst_path))
            if not os.path.exists("{}/masks/right".format(dst_path)):
                os.makedirs("{}/masks/right".format(dst_path))
            cv2.imwrite("{}/masks/left/{}".format(dst_path,img_name[0]), undistorted_left_el)
            cv2.imwrite("{}/masks/right/{}".format(dst_path,img_name[0]), undistorted_right_el)
        contours_l,hierarchy = cv2.findContours(undistorted_left_el, 1, 2)
        contours_r,hierarchy = cv2.findContours(undistorted_right_el, 1, 2)
    
    
        ####################################################################################
        # Fit ellipses around the contours to get the ellipse coordinates from 
        ####################################################################################    
        left_frame = {}
        left_frame["frame_id"] = frame[0]["frame_id"]
        rois = []
        for cnt in contours_l:
            roi = {}
            if len(cnt) >= 5:
                ellipse = cv2.fitEllipse(cnt)
                roi["centroid_x"] = ellipse[0][0]
                roi["centroid_y"] = ellipse[0][1]
                roi["ellipse_minor"] = ellipse[1][0]
                roi["ellipse_major"] = ellipse[1][1]
                roi["ellipse_angle"] = ellipse[2]
                if draw_ellipses:
                    # Draw ellipses
                    dst_l = cv2.ellipse(dst_l,ellipse,(0,255,0),3)
                rois.append(roi)
        left_frame["n_centroids"] = len(rois)
        left_frame["rois"] = rois
        updated_blobs["cameras"]["left camera"].append(left_frame)
            
        right_frame = {}
        right_frame["frame_id"] = frame[1]["frame_id"]
        rois = []
        for cnt in contours_r:
            roi = {}
            if len(cnt) >= 5:
                ellipse = cv2.fitEllipse(cnt)
                roi["centroid_x"] = ellipse[0][0]
                roi["centroid_y"] = ellipse[0][1]
                roi["ellipse_minor"] = ellipse[1][0]
                roi["ellipse_major"] = ellipse[1][1]
                roi["ellipse_angle"] = ellipse[2]
                if draw_ellipses:
                    # Draw ellipses
                    dst_r = cv2.ellipse(dst_r,ellipse,(0,255,0),3)
                rois.append(roi)
        right_frame["n_centroids"] = len(rois)
        right_frame["rois"] = rois
        updated_blobs["cameras"]["right camera"].append(right_frame)
    
        ####################################################################################
        # Save undistorted images at destination folder
        ####################################################################################
        cv2.imwrite("{}/images/left/{}".format(dst_path,img_name[0]), dst_l)
        cv2.imwrite("{}/images/right/{}".format(dst_path,img_name[0]), dst_r)
      
    ####################################################################################
    # Save json with updated ellipse coordinates at destination folder
    ####################################################################################
    with open('{}/blobs.json'.format(dst_path), 'w') as bf:
        json.dump(updated_blobs, bf, indent=2)
        

    
if __name__=="__main__":
    import argparse

    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('-root-path', action='store', required=True)
    my_parser.add_argument('-dst-path', action='store', required=True)
    my_parser.add_argument('-save-masks', action='store')
    my_parser.add_argument('-draw-ellipses', action='store')
    
    args = my_parser.parse_args()
    undistort_images(args.root_path, args.dst_path, args.save_masks, args.draw_ellipses)
